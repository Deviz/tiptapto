import json
import os


class DataManager:
    def __init__(self, default_data: dict, path='data.json'):
        self.path = path

        if not os.path.isfile(path):
            self.update_data(default_data)

        self.data = self.get_data()

    # TODO: advanced check if data is corrupted and fix it
    def is_data_valid(self):
        pass

    def get_data(self) -> dict:
        with open(self.path, 'r') as json_file:
            return json.load(json_file)

    def update_data(self, data: dict = None):
        if data is None:
            data = self.data

        with open(self.path, 'w') as json_file:
            json.dump(data, json_file)

    def update_attribute(self, attr: str, val: str):
        try:
            self.data[attr] = val
            self.update_data(self.data)
        except KeyError:
            raise Exception('DataManager: Key "' + attr + '" does not exist.')
