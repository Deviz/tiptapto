from config import *
from math import inf as infinity
from random import choice


class GameManager:
    HUMAN = -1
    AI = +1

    def __init__(self, board_width, board_height, tags_to_win):
        self.height = board_height
        self.width = board_width
        self.tags_to_win = tags_to_win
        self.state = None
        self.turn = PLAYERS_TAGS[0].lower()

        self.ai_player = PLAYERS_TAGS[1]
        self.human_player = PLAYERS_TAGS[0]

    def get_empty_cells(self):
        empty = 0
        for i in range(self.height):
            for j in range(self.width):
                if self.state[i][j] == '':
                    empty += 1
        return empty

    def empty_cells(self):
        cells = []

        for x, row in enumerate(self.state):
            for y, cell in enumerate(row):
                if cell == '':
                    cells.append([x, y])

        return cells

    def evaluate(self):
        winner = self.get_winner()

        if winner == self.ai_player:
            score = self.AI
        elif winner == self.human_player:
            score = self.HUMAN
        else:
            score = 0

        return score

    def ai_turn(self):
        empty_cells = self.empty_cells()
        depth = len(empty_cells)

        if depth == 0 or self.get_winner() != 0:
            return

        if depth >= 8:
            x, y = choice(empty_cells)
        else:
            move = self.minimax(depth, self.AI)
            x, y = move[0], move[1]

        return x, y

    # This AI is very weak :(
    # TODO: transform it into alpha-beta pruning
    def minimax(self, depth, player):
        if player == self.AI:
            best = [-1, -1, -infinity]
        else:
            best = [-1, -1, +infinity]

        if depth == 0 or self.get_winner() != 0:
            score = self.evaluate()
            return [-1, -1, score]

        for cell in self.empty_cells():
            x, y = cell[0], cell[1]

            if player == self.AI:
                self.state[x][y] = PLAYERS_TAGS[1]
            else:
                self.state[x][y] = PLAYERS_TAGS[0]

            score = self.minimax(depth - 1, -player)
            self.state[x][y] = ''
            score[0], score[1] = x, y

            if player == self.AI:
                if score[2] > best[2]:
                    best = score  # max value
            else:
                if score[2] < best[2]:
                    best = score  # min value

        return best

    def update(self, state):
        self.state = state

    def update_position(self, i, j, player_tag):
        self.state[i][j] = player_tag

    def get_next_turn(self):
        if self.turn == PLAYERS_TAGS[0]:
            self.turn = PLAYERS_TAGS[1]
            return PLAYERS_TAGS[1]
        else:
            self.turn = PLAYERS_TAGS[0]
            return PLAYERS_TAGS[0]

    def get_current_turn(self):
        return self.turn

    # TODO: make algorithm more efficient (cuz efficiency > simplicity xd)
    # Loops could be merged
    def get_winner(self, state=None):
        """
        Function that checks whether someone wins and who
        Algorithm is capable of handling more than 2 players!
        :param state: state of the board
        :return: Winner PLAYER_TAG: either 'x' or 'o'
        """
        if state is not None:
            self.update(state)

        h = self.check_horizontal()
        v = self.check_vertical()
        d = self.check_diagonal()

        if h != 0:
            return h
        elif v != 0:
            return v
        elif d != 0:
            return d

        return 0

    def check_horizontal(self):
        for i in range(self.height):
            for j in range(self.width):
                for player_tag in PLAYERS_TAGS:
                    if j+self.tags_to_win <= self.width and \
                            all(p == player_tag for p in self.state[i][j:j+self.tags_to_win]):
                        # print('horizontal:', self.state[i])
                        return player_tag
        return 0

    def check_vertical(self):
        for j in range(self.width):
            col = [row[j] for row in self.state]
            for i in range(len(col)):
                for player_tag in PLAYERS_TAGS:
                    if i+self.tags_to_win <= self.height and \
                            all(p == player_tag for p in col[i:i+self.tags_to_win]):
                        # print('vertical:', col)
                        return player_tag
        return 0

    def check_diagonal(self):
        for i in range(self.height):
            for j in range(self.width):
                items_forward = []
                items_reverse = []

                for k in range(int(self.width * self.height / 2)):
                    if i+k <= self.height-1 and j+k <= self.width-1:
                        items_forward.append(
                            self.state[i+k][j+k]
                        )
                    if i+k <= self.height-1 and j-k >= 0:
                        items_reverse.append(
                            self.state[i+k][j-k]
                        )

                for k in range(len(items_forward)):
                    for player_tag in PLAYERS_TAGS:
                        if k+self.tags_to_win <= len(items_forward) and \
                                all(p == player_tag for p in items_forward[k:k+self.tags_to_win]):
                            # print('forward diagonal:', items_forward)
                            return player_tag

                for k in range(len(items_reverse)):
                    for player_tag in PLAYERS_TAGS:
                        if k+self.tags_to_win <= len(items_reverse) and \
                                all(p == player_tag for p in items_reverse[k:k+self.tags_to_win]):
                            # print('reverse diagonal:', items_reverse)
                            return player_tag
        return 0
