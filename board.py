import tkinter as tk
from tkinter import messagebox
from config import *
from game_manager import GameManager


class Board(tk.Frame):
    def __init__(self, parent, board_width, board_height, tags_to_win, nickname_label):
        super().__init__(parent)
        self.parent = parent
        self.height = board_height
        self.width = board_width
        self.nickname_label = nickname_label
        self.game = GameManager(board_width, board_height, tags_to_win)
        self.buttons = [[] for _ in range(self.height)]
        self.init_buttons()
        self.ai_turn = False

    def init_buttons(self):
        for i in range(self.height):
            self.rowconfigure(i, weight=1)
            for j in range(self.width):
                self.columnconfigure(j, weight=1)
                self.buttons[i].append(
                    tk.Button(
                        self,
                        width=3,
                        height=1,
                        relief='groove',
                        font=('Arial', 60, 'bold'),
                        command=lambda col=i, row=j:
                            self.on_button_click(col, row)
                    )
                )
                self.buttons[i][j].grid(
                    row=i,
                    column=j,
                    sticky=tk.E + tk.W + tk.N + tk.S
                )

    def on_button_click(self, i, j):
        if self.ai_turn:
            return

        self.game.update(self.get_state())
        if self.parent.controller.game_mode == 'ai':
            self.buttons[i][j].config(text=PLAYERS_TAGS[0])
            self.buttons[i][j].config(state=tk.DISABLED, disabledforeground=PLAYERS_COLORS[0])
            self.game.update_position(i, j, PLAYERS_TAGS[0])
            self.nickname_label.config(
                text='WARNING: AI might be thinking for a while!'
            )

            winner = self.game.get_winner()
            if winner != 0:
                self.give_points(data_manager.data['last_nicknames'][PLAYERS_TAGS.index(winner)])
                self.handle_play_again(winner)
                self.reset()
            elif self.board_is_full():
                self.handle_full_board()
                self.reset()
            else:
                self.ai_turn = True
                ai_turn = self.game.ai_turn()
                self.buttons[ai_turn[0]][ai_turn[1]].config(text=self.game.ai_player)
                self.buttons[ai_turn[0]][ai_turn[1]].config(state=tk.DISABLED, disabledforeground=PLAYERS_COLORS[1])
                self.game.update_position(ai_turn[0], ai_turn[1], PLAYERS_TAGS[1])

                winner = self.game.get_winner()
                if winner != 0:
                    self.handle_play_again(winner)
                    self.reset()
                elif self.board_is_full():
                    self.handle_full_board()
                    self.reset()

                self.ai_turn = False
        else:
            curr_turn = self.game.get_current_turn()

            self.buttons[i][j].config(text=curr_turn)
            self.buttons[i][j].config(state=tk.DISABLED, disabledforeground=PLAYERS_COLORS[
                PLAYERS_TAGS.index(curr_turn)
            ])

            next_turn = self.game.get_next_turn()
            self.nickname_label.config(
                text=f'{data_manager.data["last_nicknames"][PLAYERS_TAGS.index(next_turn)]} ({next_turn.upper()}) turn'
            )
            winner = self.game.get_winner(self.get_state())
            if winner != 0:
                self.give_points(data_manager.data['last_nicknames'][PLAYERS_TAGS.index(curr_turn)])
                self.handle_play_again(winner)
                self.reset()
            elif self.board_is_full():
                self.handle_full_board()
                self.reset()

    @staticmethod
    def give_points(player_name):
        for pn in data_manager.data['players']:
            if pn['name'] == player_name:
                pn['wins'] += 1

    def handle_play_again(self, winner):
        if messagebox.askyesno(
                f'{data_manager.data["last_nicknames"][PLAYERS_TAGS.index(winner)]} ({winner.upper()}) won!',
                "Exit to main menu?"
        ):
            self.parent.controller.show_frame('StartPage')

    def handle_full_board(self):
        if messagebox.askyesno('Draw!', "Exit to main menu?"):
            self.parent.controller.show_frame('StartPage')

    def reset(self):
        for i in range(self.height):
            for j in range(self.width):
                self.buttons[i][j].config(text='', state=tk.NORMAL)

    def board_is_full(self):
        return self.game.get_empty_cells() == 0

    def get_state(self):
        state = [[] for _ in range(self.height)]
        for i in range(self.height):
            for j in range(self.width):
                state[i].append(
                    self.buttons[i][j]['text']
                )
        return state
