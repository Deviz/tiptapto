import tkinter as tk
from tkinter import font as tkfont
from pages import *

pages = (
    StartPage,
    GamePage,
    ScoreboardPage
)


class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.base_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.rowconfigure(0, weight=1)
        container.columnconfigure(0, weight=1)

        self.frames = {}
        for F in pages:
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky='nsew')

        self.show_frame('StartPage')

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()
        frame.on_frame_show()


if __name__ == '__main__':
    app = App()
    app.rowconfigure(0, weight=1)
    app.columnconfigure(0, weight=1)
    app.title('TipTapTo')
    app.minsize(500, 525)
    app.mainloop()
