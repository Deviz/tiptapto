from data_manager import DataManager

data_manager = DataManager(
    {
        'board_settings': {
            'height': 3,
            'width': 3,
            'tags_to_win': 3
        },
        'players_tags': ['o', 'x'],
        'players_colors': ['#0052cc', '#000'],
        'last_nicknames': ['', ''],
        'last_play_mode': 'ai',
        'players': []
    }
)

# Board settings:
DEFAULT_BOARD_HEIGHT = data_manager.data['board_settings']['height']
DEFAULT_BOARD_WIDTH = data_manager.data['board_settings']['width']
TAGS_TO_WIN = data_manager.data['board_settings']['tags_to_win']

# Player settings:
PLAYERS_TAGS = data_manager.data['players_tags']  # First one starts the game
PLAYERS_COLORS = data_manager.data['players_colors']  # Colors of players starting with the first one
