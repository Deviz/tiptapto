import tkinter as tk
from tkinter import ttk
from tkinter.colorchooser import askcolor
from config import *


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.parent = parent
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        self.notification = None
        self.controller.width = DEFAULT_BOARD_HEIGHT
        self.controller.height = DEFAULT_BOARD_WIDTH
        self.controller.tags_to_win = TAGS_TO_WIN

        self.main_controls = MainControls(self)
        self.main_controls.grid(row=0, column=0, columnspan=2)

        self.create_separator(1)
        self.create_separator(3)

        self.dimensions = BoardControls(self)
        self.dimensions.grid(row=4, column=0)

        self.create_separator(5)

        self.players_settings = list()
        j = 0
        for i, player_tag in enumerate(PLAYERS_TAGS):
            self.players_settings.append(
                PlayerSetting(self, player_tag, PLAYERS_COLORS[i])
            )
            self.players_settings[i].grid(row=i + j + 6, column=0)
            if i != len(PLAYERS_TAGS)-1:
                self.create_separator(i + j + 7)
                j += 1

        self.game_mode_controls = GameModeControls(self)
        self.game_mode_controls.grid(row=2, column=0, columnspan=2)

    def create_separator(self, row):
        ttk.Separator(self, orient='horizontal').grid(row=row, columnspan=2, pady=10, sticky='ew')

    @staticmethod
    def get_notification_row():
        return len(PLAYERS_TAGS) * 2 + 6

    def on_frame_show(self):
        pass


class MainControls(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.parent.controller.new_game_btn = tk.Button(
            self,
            state=tk.DISABLED,
            text='Start game',
            font=self.parent.controller.base_font,
            bg='#0052cc',
            fg='#ffffff',
            command=lambda: self.on_new_game_start()
        )
        self.parent.controller.new_game_btn.grid(row=0, columnspan=2, padx=5, pady=5, sticky='nsew')

        tk.Button(self, text='Scoreboard', font=self.parent.controller.base_font, bg='#0052cc', fg='#ffffff',
                  command=lambda: self.parent.controller.show_frame('ScoreboardPage')) \
            .grid(row=1, column=0, columnspan=2, padx=5, pady=5, sticky='nsew')

    def on_new_game_start(self):
        self.parent.controller.show_frame('GamePage')

        players = data_manager.data['players']
        last_players = data_manager.data['last_nicknames']
        for last_player in last_players:
            if not any(p['name'] == last_player for p in players) and last_player != '':
                players.append({'name': last_player, 'wins': 0})

        data_manager.data['players'] = players
        data_manager.update_data()


class GameModeControls(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent

        tk.Label(self, text='Game mode:', font=self.parent.controller.base_font).grid(row=0, column=0, sticky='nsew')
        self.game_mode_btn = tk.Button(
            self,
            text=data_manager.data['last_play_mode'].upper(),
            width=12,
            command=lambda: self.toggle()
        )
        self.game_mode_btn.grid(row=0, column=1)
        self.update_game_mode()

    def update_game_mode(self):
        if data_manager.data['last_play_mode'] == 'human':
            self.parent.players_settings[0].player_label.config(
                text=f'Player O nickname:'
            )
            self.parent.players_settings[0].color_label.config(
                text=f'Player O color:'
            )
            if self.parent.players_settings[1] is None:
                self.parent.players_settings[1] = PlayerSetting(self.parent, PLAYERS_TAGS[1], PLAYERS_COLORS[1])
                self.parent.players_settings[1].grid(row=9, column=0)
            self.parent.controller.game_mode = 'human'

        else:
            self.parent.players_settings[0].player_label.config(text=f'Your nickname (O):')
            self.parent.players_settings[0].color_label.config(text=f'Your color (O):')
            self.parent.players_settings[1].destroy()
            self.parent.players_settings[1] = None
            self.parent.controller.game_mode = 'ai'

    def toggle(self):
        if self.game_mode_btn['text'] == 'AI':
            self.game_mode_btn.config(text='HUMAN')
            data_manager.data['last_play_mode'] = 'human'

        else:
            self.game_mode_btn.config(text='AI')
            data_manager.data['last_play_mode'] = 'ai'

        data_manager.update_data()
        self.parent.check_start_game_btn()
        self.update_game_mode()


class BoardControls(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.parent.controller.width = DEFAULT_BOARD_WIDTH
        self.parent.controller.height = DEFAULT_BOARD_HEIGHT
        self.init_dimensions_controls()

    def update_width(self, sb):
        self.parent.controller.width = sb.get()
        data_manager.data['board_settings']['width'] = sb.get()
        data_manager.update_data()

    def update_height(self, sb):
        self.parent.controller.height = sb.get()
        data_manager.data['board_settings']['height'] = sb.get()
        data_manager.update_data()

    def update_tags(self, sb):
        self.parent.controller.tags_to_win = sb.get()
        data_manager.data['board_settings']['tags_to_win'] = sb.get()
        data_manager.update_data()

    def init_dimensions_controls(self):
        tk.Label(self, text='Board width:', font=self.parent.controller.base_font).grid(row=0, column=0, sticky='nsew')
        v1 = tk.StringVar()
        w = tk.Spinbox(self, textvariable=v1, from_=3, to_=15)
        v1.set(self.parent.controller.width)
        w.config(command=lambda sb=w: self.update_width(sb))
        w.grid(row=0, column=1, pady=5, sticky='nsew')

        tk.Label(self, text='Board height:', font=self.parent.controller.base_font).grid(row=1, column=0, sticky='nsew')
        v2 = tk.StringVar()
        h = tk.Spinbox(self, textvariable=v2, from_=3, to_=15)
        v2.set(self.parent.controller.height)
        h.config(command=lambda sb=h: self.update_height(sb))
        h.grid(row=1, column=1, pady=5, sticky='nsew')

        tk.Label(self, text='Tags to win:', font=self.parent.controller.base_font).grid(row=2, column=0, sticky='nsew')
        v3 = tk.StringVar()
        tags_to_win = tk.Spinbox(self, textvariable=v3, from_=3, to_=15)
        v3.set(self.parent.controller.tags_to_win)
        tags_to_win.config(command=lambda sb=tags_to_win: self.update_tags(sb))
        tags_to_win.grid(row=2, column=1, pady=5, sticky='nsew')


class PlayerSetting(tk.Frame):
    def __init__(self, parent, player_tag, player_color):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.player_tag = player_tag
        self.player_color = player_color

        self.player_label = None
        self.player_entry = None
        self.color_label = None
        self.color_chooser = None

        self.init_player_nick(0)
        self.init_player_color(1)
        self.parent.check_start_game_btn = self.check_start_game_btn
        self.check_start_game_btn()

    def init_player_nick(self, row):
        self.player_label = tk.Label(
            self,
            text=f'Player {self.player_tag.upper()} nickname:',
            font=self.parent.controller.base_font
        )
        self.player_label.grid(
            row=row,
            column=0,
            sticky='nsew'
        )

        sv = tk.StringVar(value=data_manager.data['last_nicknames'][PLAYERS_TAGS.index(self.player_tag)])
        sv.trace('w', lambda name, index, mode, stv=sv: self.nick_update_callback(stv.get()))
        # TODO: validate nickname length
        self.player_entry = tk.Entry(self, textvariable=sv)
        self.player_entry.grid(row=row, column=1, pady=5, sticky='nsew')

    def init_player_color(self, row):
        self.color_label = tk.Label(
            self,
            text=f'Player {self.player_tag.upper()} color:',
            font=self.parent.controller.base_font
        )
        self.color_label.grid(
            row=row, column=0, sticky='nsew'
        )
        self.color_chooser = tk.Button(self, background=PLAYERS_COLORS[PLAYERS_TAGS.index(self.player_tag)])
        self.color_chooser.config(command=lambda btn=self.color_chooser: self.color_chooser_callback(btn))
        self.color_chooser.grid(row=row, column=1, pady=5, sticky='nsew')

    def nick_update_callback(self, nick):
        index = PLAYERS_TAGS.index(self.player_tag)
        data_manager.data['last_nicknames'][index] = nick
        data_manager.update_data()
        self.check_start_game_btn()

    def check_start_game_btn(self):
        if (data_manager.data['last_play_mode'] == 'human'
            and any(n == '' for n in data_manager.data['last_nicknames'])) or \
                (data_manager.data['last_play_mode'] == 'ai' and data_manager.data['last_nicknames'][0] == ''):
            self.parent.controller.new_game_btn['state'] = tk.DISABLED
            self.show_notification()
        else:
            self.parent.controller.new_game_btn['state'] = tk.NORMAL
            self.hide_notification()

    def show_notification(self):
        if self.parent.notification is None:
            self.parent.notification = tk.Label(
                self.parent,
                text='To start the game you must fill in blanks!',
                foreground='red'
            )
            self.parent.notification.grid(row=self.parent.get_notification_row(), columnspan=2, pady=5)

    def hide_notification(self):
        if self.parent.notification is not None:
            self.parent.notification.destroy()
            self.parent.notification = None

    def color_chooser_callback(self, color_button):
        hex_color = askcolor()[1]
        color_button.config(background=hex_color)
        index = PLAYERS_TAGS.index(self.player_tag)
        PLAYERS_COLORS[index] = hex_color

        data_manager.data['players_colors'][index] = hex_color
        data_manager.update_data()
