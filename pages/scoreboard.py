import tkinter as tk
from config import data_manager


class ScoreboardPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.parent = parent
        self.go_back_btn(1)

    def go_back_btn(self, row):
        tk.Button(self, text='Go back', font=self.controller.base_font, bg='#0052cc', fg='#ffffff',
                  command=lambda: self.controller.show_frame('StartPage')) \
            .grid(row=row, columnspan=2, padx=5, pady=5, sticky='nsew')

    def on_frame_show(self):
        lb = tk.Listbox(self)

        sb = tk.Scrollbar(lb, orient='vertical')
        sb.config(command=lb.yview)
        sb.pack(side="right", fill="y")

        lb.config(yscrollcommand=sb.set)
        lb.grid(row=0, column=0, sticky='nsew')

        players = sorted(data_manager.data['players'], key=lambda k: k['wins'], reverse=True)
        if len(players) == 0:
            lb.insert(1, 'Play to be the number one!')
        else:
            lb.delete(1)
            for i in range(len(players)):
                lb.insert(i + 1, f'{i + 1}. {players[i]["name"]}: '
                                 f'{players[i]["wins"]} {self.win_word(players[i]["wins"])}')

    @staticmethod
    def win_word(wins):
        if str(wins)[-1] == '1':
            return 'win'
        else:
            return 'wins'
