from .game import GamePage
from .start import StartPage
from .scoreboard import ScoreboardPage
