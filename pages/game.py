import tkinter as tk
from config import *
from board import Board


class GamePage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.parent = parent
        self.board = None

    def on_frame_show(self):
        for li in self.grid_slaves():
            li.destroy()

        nickname_label = tk.Label(
            self,
            text=f'{data_manager.data["last_nicknames"][0]} ({PLAYERS_TAGS[0].upper()}) turn'
        )
        nickname_label.grid(row=1, column=0)

        self.board = Board(
            self,
            int(self.controller.width),
            int(self.controller.height),
            int(self.controller.tags_to_win),
            nickname_label
        )
        self.board.grid(row=0, column=0)
